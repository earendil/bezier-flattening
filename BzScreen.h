#ifndef BZSCREEN_H
#define BZSCREEN_H

#include <QWidget>
#include <QPaintEvent>
#include <QBrush>
#include <QPen>
#include <QPainter>
#include <QPainterPath>
#include <QPixmap>

class BzScreen : public QWidget
{
    Q_OBJECT

public:
    BzScreen(QWidget *parent = nullptr);
    void paintEvent(QPaintEvent *);

    QBrush brush;
    QPen pen;
    QPainterPath path;
    QPainter painter;
   void paintEvent2 ( QPaintEvent * event );
   void wheelEvent ( QWheelEvent * event );
   qreal scale;


};
#endif // BZSCREEN_H
