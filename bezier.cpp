#include "widget.h"
#include "BzScreen.h"
#include <QWidget>
#include <QGridLayout>
#include <QPainter>
#include <QPainterPath>
#include <private/qbezier_p.h>
#include <private/qpathclipper_p.h>
#include <private/qdatabuffer_p.h>

#include <iostream>
#include <QTransform>

using namespace std;

BzScreen::BzScreen( QWidget *parent) : QWidget(parent)
{
    pen.setColor(QColor(40,80,120));
    pen.setWidth(1);

    QPainterPath path;

}

string retType(int enum_num){
    if(enum_num == 0){
        return "moveTo";
    }
    else {
        return (enum_num ==1) ? "lineTo" : "curveTo";
    }
}

void myAddToPolygon(QDataBuffer<QPointF> &polygon, qreal bezier_flattening_threshold,QBezier *bzr)
{
    QBezier beziers[10];
    int levels[10];
    beziers[0] = *bzr;
    levels[0] = 9;
    int top = 0;

    while (top >= 0) {
        QBezier *b = &beziers[top];
        // check if we can pop the top bezier curve from the stack
        qreal y4y1 = b->y4 - b->y1;
        qreal x4x1 = b->x4 - b->x1;
        qreal l = qAbs(x4x1) + qAbs(y4y1);
        qreal d;
        if (l > 1.) {
            d = qAbs( (x4x1)*(b->y1 - b->y2) - (y4y1)*(b->x1 - b->x2) )
                + qAbs( (x4x1)*(b->y1 - b->y3) - (y4y1)*(b->x1 - b->x3) );
        } else {
            d = qAbs(b->x1 - b->x2) + qAbs(b->y1 - b->y2) +
                qAbs(b->x1 - b->x3) + qAbs(b->y1 - b->y3);
            l = 1.;
        }
        if (d < bezier_flattening_threshold * l || levels[top] == 0) {
            // good enough, we pop it off and add the endpoint
            polygon.add(QPointF(b->x4, b->y4));
            --top;
        } else {
            // split, second half of the polygon goes lower into the stack
            std::tie(b[1], b[0]) = b->split();
            levels[top + 1] = --levels[top];
            ++top;
        }
    }
}

QDataBuffer<QPointF> m_points(0);
QDataBuffer<QPathSegments::Segment> m_segments(0);
QDataBuffer<QPathSegments::Intersection> m_intersections(0);
int m_pathId = 0;

static inline bool fuzzyIsNull(qreal d)
{
    if (sizeof(qreal) == sizeof(double))
        return qAbs(d) <= 1e-12;
    else
        return qAbs(d) <= 1e-5f;
}


static inline bool comparePoints(const QPointF &a, const QPointF &b)
{
    return fuzzyIsNull(a.x() - b.x())
           && fuzzyIsNull(a.y() - b.y());
}

static bool isLine(const QBezier &bezier)
{
    const bool equal_1_2 = comparePoints(bezier.pt1(), bezier.pt2());
    const bool equal_2_3 = comparePoints(bezier.pt2(), bezier.pt3());
    const bool equal_3_4 = comparePoints(bezier.pt3(), bezier.pt4());

    // point?
    if (equal_1_2 && equal_2_3 && equal_3_4)
        return true;

    if (comparePoints(bezier.pt1(), bezier.pt4()))
        return equal_1_2 || equal_3_4;

    return (equal_1_2 && equal_3_4) || (equal_1_2 && equal_2_3) || (equal_2_3 && equal_3_4);
}

inline const QLineF lineAt(int index)
{
    const  QPathSegments::Segment &segment = m_segments.at(index);
    return QLineF(m_points.at(segment.va), m_points.at(segment.vb));
}



void flatten(const QPainterPath &path,QPainterPath &path2, float manualThreshold =20)
{
    int firstSegment = m_segments.size();


    bool hasMoveTo = false;
    int lastMoveTo = 0;
    int last = 0;
    for (int i = 0; i < path.elementCount(); ++i) {
        int current = m_points.size();

        QPointF currentPoint;
        if (path.elementAt(i).type == QPainterPath::CurveToElement)
            currentPoint = path.elementAt(i+2);
        else
            currentPoint = path.elementAt(i);

        if (i > 0 && comparePoints(m_points.at(lastMoveTo), currentPoint))
            current = lastMoveTo;
        else
            m_points << currentPoint;

        switch (path.elementAt(i).type) {
        case QPainterPath::MoveToElement:
            if (hasMoveTo && last != lastMoveTo && !comparePoints(m_points.at(last), m_points.at(lastMoveTo)))
                m_segments << QPathSegments::Segment(m_pathId, last, lastMoveTo);
            hasMoveTo = true;
            last = lastMoveTo = current;
            break;
        case QPainterPath::LineToElement:
            m_segments << QPathSegments::Segment(m_pathId, last, current);
            last = current;
            break;
        case QPainterPath::CurveToElement:
            {
                QBezier bezier = QBezier::fromPoints(m_points.at(last), path.elementAt(i), path.elementAt(i+1), path.elementAt(i+2));
                if (isLine(bezier)) {
                    m_segments << QPathSegments::Segment(m_pathId, last, current);
                } else {
//                    QRectF bounds = bezier.bounds();

                    // threshold based on similar algorithm as in qtriangulatingstroker.cpp
//                    float threshold = qMin<float>(64, qMax(bounds.width(), bounds.height()) * (2 * qreal(3.14) / 6));

//                    if (threshold < 3) threshold = 3;

//                    manualThreshold = 8; // bigger the threshold, better the curve ????

                    qreal one_over_threshold_minus_1 = qreal(1) / (manualThreshold - 1);



                    for (int t = 1; t < manualThreshold - 1; ++t) {
                        currentPoint = bezier.pointAt(t * one_over_threshold_minus_1);

                        int index = m_points.size();
                        m_segments << QPathSegments::Segment(m_pathId, last, index);
                        path2.lineTo(currentPoint);
                        last = index;

                        m_points << currentPoint;
                    }

                    m_segments << QPathSegments::Segment(m_pathId, last, current);
                }
            }
            last = current;
            i += 2;
            break;
        default:
            Q_ASSERT(false);
            break;
        }
    }

    if (hasMoveTo && last != lastMoveTo && !comparePoints(m_points.at(last), m_points.at(lastMoveTo)))
        m_segments << QPathSegments::Segment(m_pathId, last, lastMoveTo);

    for (int i = firstSegment; i < m_segments.size(); ++i) {
        const QLineF line = lineAt(i);

        qreal x1 = line.p1().x();
        qreal y1 = line.p1().y();
        qreal x2 = line.p2().x();
        qreal y2 = line.p2().y();

        if (x2 < x1)
            qSwap(x1, x2);
        if (y2 < y1)
            qSwap(y1, y2);

        m_segments.at(i).bounds = QRectF(x1, y1, x2 - x1, y2 - y1);
    }

    ++m_pathId;
}

void BzScreen::paintEvent(QPaintEvent *e){

    //control points
    QPoint cp1(40,10);
    QPoint cp2(80,40);
    QPoint cp3(60,75);
    QPoint cp4(20,65);

    // paths
        QPainter painter(this);
        QPainterPath quadPath;
        quadPath.moveTo(cp1);
        quadPath.quadTo(cp2,cp4);
        QPainterPath cubicPath;
        cubicPath.moveTo(cp1);
        cubicPath.cubicTo(cp2,cp3,cp4);

 //       cubicPath.moveTo(20, 65);
        QPainterPath cubicCopy = cubicPath;

        QPainterPath quadMarkers;
        QPainterPath cubicMarkers;
        QPainterPath pathMarkers;

        //colors and pens
        QPen redpen(QColor("red"));
        redpen.setWidthF(0.4);
        QPen greenPen(QColor("green"));
        greenPen.setWidthF(0.6);
        QPen bluePen(QColor("blue"));
        bluePen.setWidthF(0.8);

        // init elements and markers

//            cout<<"init elements quad"<<endl;

        double sideLength = 2;
        double sideLengthHalf = sideLength / 2;


            for (int i=0;i < quadPath.elementCount() ;i++ ) {
                QPainterPath::Element curr = quadPath.elementAt(i);
                cout<<retType(curr.type)<<" x: "<<curr.x<<" y: "<<curr.y<<endl;
                quadMarkers.addEllipse(curr.x - sideLengthHalf, curr.y - sideLengthHalf, sideLength, sideLength);
            }
//            cout<<"-----------------------------"<<endl;
//            cout<<"init elements cubic"<<endl;

            for (int i=0;i < cubicPath.elementCount() ;i++ ) {
                QPainterPath::Element curr = cubicPath.elementAt(i);
                cout<<retType(curr.type)<<" x: "<<curr.x<<" y: "<<curr.y<<endl;
                cubicMarkers.addEllipse(curr.x - sideLengthHalf, curr.y - sideLengthHalf, sideLength, sideLength);
            }


            //operations
            QPainterPath quadCopy = quadPath;

            path.moveTo(cp1);
            path |= quadCopy;
            path |= cubicCopy;
            path.moveTo(cp4);

            //adding flattened bezier

            QPainterPath flbz;


            flbz.moveTo(cp1);

            int thresh = 20;

            flatten(cubicCopy,flbz,thresh);
            flbz.lineTo(cp4);



            for (int i=0;i < flbz.elementCount() ;i++ ) {
                QPainterPath::Element curr = flbz.elementAt(i);
                pathMarkers.addEllipse(curr.x - sideLengthHalf, curr.y - sideLengthHalf, sideLength, sideLength);
            }


            //scale
            QTransform transform;
            transform.scale(8,8);
            painter.setTransform(transform);
            painter.scale(scale,scale);

            // Add everything to Painter

            painter.setPen(bluePen);
            painter.drawPath(cubicPath);
            painter.drawPath(cubicMarkers);

            painter.setPen(redpen);
            painter.drawPath(flbz);

            painter.setPen(greenPen);
            painter.drawPath(pathMarkers);

}


void BzScreen::paintEvent2 ( QPaintEvent * event )
{
    QPainter p;
    p.scale(scale,scale);

// paint here
}
void BzScreen::wheelEvent ( QWheelEvent * event )
{
    scale+=(event->angleDelta().y()); //or use any other step for zooming
}




